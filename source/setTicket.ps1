

$TARGET = 'User'
$TICKET_VAR_NAME = 'CI_TICKET'
$PREV_TICKET_VAR_NAME = 'CI_PREV_TICKET'
function setTicket {
    $ticket = $args[0]
    If ($ticket -eq $nul) { Write-Host "Please enter ticket name after the command" }
    
    else {
        Write-Host "Setting environment variables ..."
        $prevTicket = [System.Environment]::GetEnvironmentVariable($TICKET_VAR_NAME, $TARGET)
        try {
            [System.Environment]::SetEnvironmentVariable($PREV_TICKET_VAR_NAME, $prevTicket, $TARGET)

            [System.Environment]::SetEnvironmentVariable($TICKET_VAR_NAME, $ticket, $TARGET)
            Write-Host "Done!"
            Write-Host "Current ticket is set to: " $ticket
            Write-Host "Previous ticket is set to: " $prevTicket
            Write-Warning "Please use a new PowerShell window/process for changes to be applied"
        }
        catch {
            Write-Warning -Message "Something went wrong..."
            Write-Error $Error[0]
        }
    }
}
function getTicket {
    $prevTicket = [System.Environment]::GetEnvironmentVariable($PREV_TICKET_VAR_NAME, $TARGET)
    $ticket = [System.Environment]::GetEnvironmentVariable($TICKET_VAR_NAME, $TARGET)
    Write-Host "Current ticket: " $ticket
    Write-Host "Previous ticket: " $prevTicket
}