
import requests
import json
import os
from dotenv import load_dotenv
from datetime import datetime, date
from http import HTTPStatus
import argparse

PATH_FOLDER_NAME = 'ci-auto-log'
FULL_DAY_HOURS = 7.6
ERROR_COLOR = '\033[91m'
END_COLOR = '\033[0m'


def logHours(issueKey, timeSpentSeconds, startDate, startTime, description, authorAccountId):
    BASE_URL = 'https://api.tempo.io/core/3'
    WORK_LOGS = '/worklogs'
    TOKEN = os.getenv('TOKEN')

    HEADERS = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer {0}'.format(TOKEN)
    }

    log = {
        "issueKey": issueKey,
        "timeSpentSeconds": timeSpentSeconds,
        "billableSeconds": timeSpentSeconds,
        "startDate": startDate,
        "startTime": startTime,
        "description": description,
        "authorAccountId": authorAccountId
    }
    data = json.dumps(log)
    response = requests.post(
        url=BASE_URL + WORK_LOGS,
        headers=HEADERS,
        data=data
    )
    if(response.status_code != HTTPStatus.OK):
        print(ERROR_COLOR + 'Something went wrong:' + END_COLOR)
        print(response.content)
        print(response.text)
    else:
        print('Done!')


def getTimeSpent(hours):
    secondsPerHour = 3600
    return float(hours)*secondsPerHour


def getToday():
    return datetime.today().replace(hour=8, minute=0, second=0, microsecond=0)


def getDate(d):
    if(isinstance(d, date)):
        return d
    try:
        return datetime.strptime(d, '%Y-%m-%d').replace(hour=8, minute=0, second=0, microsecond=0)
    except ValueError as e:
        print(ERROR_COLOR + "Timme data does not match format YYYY-MM-DD" + END_COLOR)
        print(e)


def getStartDate(dateTime):
    return dateTime.strftime('%Y-%m-%d')


def getStartTime(dateTime):
    return dateTime.strftime('%H:%M:%S')


def main():
    try:
        paths = os.environ['Path'].split(os.pathsep)
        auto_log_path = [s for s in paths if PATH_FOLDER_NAME in s][0]
        dotenv_path = os.path.join(auto_log_path, '.env')
        load_dotenv(dotenv_path=dotenv_path)
    except Exception as e:
        print(ERROR_COLOR + "Cannot find program path or .env file" + END_COLOR)
        print(e)

    try:
        TICKET_ENV_VAR_NAME = os.getenv('TICKET_ENV_VAR_NAME')
        if os.getenv(TICKET_ENV_VAR_NAME) == None:
            raise Exception()
    except Exception as e:
        print(ERROR_COLOR + "Cannot find " + TICKET_ENV_VAR_NAME +
              " in environment variables" + END_COLOR)

    TARGET_ENV = os.getenv('TARGET_ENV')
    parser = argparse.ArgumentParser()
    parser.add_argument("-ticket", "--ticket",
                        help="Ticket/Issue ID to be logged. Default value is stored in " +
                        TARGET_ENV + "'s environmental variable " + TICKET_ENV_VAR_NAME,
                        default=os.getenv(TICKET_ENV_VAR_NAME))
    parser.add_argument("-hours", "--hours",
                        help="Hours to be logged. Default value is " +
                        str(FULL_DAY_HOURS),
                        default=FULL_DAY_HOURS)
    parser.add_argument("-date", "--date",
                        help="Date (YYYY-MM-DD) to be logged. Default value is " +
                        str(getToday()),
                        default=getToday())
    args = parser.parse_args()

    account_id = os.getenv('ACCOUNT_ID')
    description = 'Working on ' + args.ticket

    print('Ticket: ' + args.ticket)
    print('Time Spent: ' + str(args.hours))
    print('Date: ' + getStartDate(getDate(args.date)))
    print('Logging hours...')
    logHours(args.ticket, getTimeSpent(args.hours), getStartDate(getDate(args.date)),
             getStartTime(getToday()), description, account_id)


if __name__ == "__main__":
    main()
